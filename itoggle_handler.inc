<?php

/**
 * @file
 * Vieww handlers for iToggle
 */

/**
 * A handler to provide a custom field
 *
 * @ingroup views_field_handlers
 */
abstract class itoggle_handler extends views_handler_field {

  function init(&$view, &$options) {
    drupal_add_css(drupal_get_path('module', 'itoggle') . '/itoggle.css');
    drupal_add_js(drupal_get_path('module', 'itoggle') . '/engage.itoggle.js');
    drupal_add_js(drupal_get_path('module', 'itoggle') . '/itoggle.js');
    parent::init($view, $options);
  }

  function query() {
    // Do nothing, as this handler does not need to do anything to the query itself.
  }

  /**
   * Return markup for itoggle checkbox
   * 
   * @param mixed $id
   * @param string $type
   * @param bool $checked
   * @return string
   */
  protected function _itoggle_render($id, $type, $checked) {
    $checked = ($checked === TRUE) ? 'checked="checked"' : '';
    $element_id = "itoggle_{$type}_{$id}";

    return <<<HTML
<div class="itoggle-wrapper" data-id="{$id}" data-type="{$type}">
  <input type="checkbox" id="{$element_id}" {$checked} />
</div>
HTML;
  }

}

/**
 * A handler to provide a custom iToggle Node Status field
 *
 * @ingroup views_field_handlers
 */
class itoggle_handler_field_node_status extends itoggle_handler {

  /**
   * Render the trigger field and its linked popup information.
   */
  function render($values) {
    if (isset($values->nid)) {
      if (!isset($values->node_status)) {
        $node = node_load($values->nid);
        $values->node_status = $node->status;
      }

      $type = 'node_status';
      $id = $values->nid;
      $checked = $values->node_status == 1;
      return $this->_itoggle_render($id, $type, $checked);
    }
    return NULL;
  }

}

/**
 * A handler to provide a custom iToggle Node Promote field
 *
 * @ingroup views_field_handlers
 */
class itoggle_handler_field_node_promote extends itoggle_handler {

  /**
   * Render the trigger field and its linked popup information.
   */
  function render($values) {
    if (isset($values->nid)) {
      if (!isset($values->node_promote)) {
        $node = node_load($values->nid);
        $values->node_promote = $node->promote;
      }

      $type = 'node_promote';
      $id = $values->nid;
      $checked = $values->node_promote == 1;
      return $this->_itoggle_render($id, $type, $checked);
    }
    return NULL;
  }

}

/**
 * A handler to provide a custom iToggle Node Sticky field
 *
 * @ingroup views_field_handlers
 */
class itoggle_handler_field_node_sticky extends itoggle_handler {

  /**
   * Render the trigger field and its linked popup information.
   */
  function render($values) {
    if (isset($values->nid)) {
      if (!isset($values->node_sticky)) {
        $node = node_load($values->nid);
        $values->node_sticky = $node->sticky;
      }

      $type = 'node_sticky';
      $id = $values->nid;
      $checked = $values->node_sticky == 1;
      return $this->_itoggle_render($id, $type, $checked);
    }
    return NULL;
  }

}

/**
 * A handler to provide a custom iToggle User Status field
 *
 * @ingroup views_field_handlers
 */
class itoggle_handler_field_user_status extends itoggle_handler {

  /**
   * Render the trigger field and its linked popup information.
   */
  function render($values) {
    if (isset($values->uid)) {
      if (!isset($values->users_status)) {
        $user = user_load($values->uid);
        $values->users_status = $user->status;
      }

      $type = 'user_status';
      $id = $values->uid;
      $checked = $values->users_status == 1;
      return $this->_itoggle_render($id, $type, $checked);
    }
    return NULL;
  }

}