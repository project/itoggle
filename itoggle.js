Drupal.behaviors.itoggle = {
  attach: function(context, settings) {
    var $toggle = jQuery('div.itoggle-wrapper', context);
    
    if ($toggle.length) {
      $toggle.each(function(){
        var $t = jQuery(this),
        id = $t.attr('data-id'),
        type = $t.attr('data-type');

        // @TODO should we use once()
        $t.find('input').iToggle({
          keepLabel: false,
          // @TODO easing
          onClickOn: function(){
            jQuery.getJSON(Drupal.settings.basePath + 'js/itoggle/' + type + '/' + id + '/1', function(response){
              if (!response.ok || response.ok !== true) {
                // @TODO rethink this as can cause infinite loops
                jQuery('label.itoggle', context).trigger('click');
              }
            });
          },
          onClickOff: function(){
            jQuery.getJSON(Drupal.settings.basePath + 'js/itoggle/' + type + '/' + id + '/0', function(response){
              if (!response.ok || response.ok !== true) {
                // @TODO rethink this as can cause infinite loops
                jQuery('label.itoggle', context).trigger('click');
              }
            });
          }
        });
      });
    }
  }
};